'use strict';

(function(){
    let btn = document.getElementById("play"),
        firstBlock = document.querySelector('#first-line'),
        secondBlock = document.querySelector('#second-line'),
        thirdBlock = document.querySelector('#third-line');
    let items = 0;
    // refactor data
    let newData = [];
    data.forEach(item => {
        newData.push({
            url: validateUrl(item.url),
            name: validateName(item.name),
            description: validateDesc(item.description),
            date: validateDate(item.date)
        })
    });
    // validate url
    function validateUrl(url) {
        if (url.includes('http://')) {
            return url;
        } else {
            return `http://${url}`;
        }
    }
    // validate the name
    function validateName(name) {
        if (name.length > 0) {
            return name[0].toUpperCase()+name.slice(1).toLowerCase();
        }
    }
    // validate the description
    function validateDesc(desc) {
        if (desc.length >= 15) {
            return desc.substr(0, 15) + '...'
        }
    }
    // validate the date
    function validateDate(date) {
        return moment(date).format('YYYY/M/DD h:mm');
    }
    // Init function
    function init() {

        let typeSelect = document.getElementById('type-selector').value;
        let lineSelect = document.getElementById('line-selector').value;
        // check the number of gallery items
        if (lineSelect === '0') {
            items = newData.length;
        } else if (lineSelect === '1') {
            items = 3;
        } else {
            items = 6;
        }
        // check the gallery creation type
        if (typeSelect === '1' ) {
            replaceGallery(items);
        } else if (typeSelect === '2') {
            tempStrGallery(items);
        } else if (typeSelect === '3') {
            createGallery(items);
        }
    }
    // replace type
    function replaceGallery(items) {
        firstBlock.innerHTML = '';
        for (let i = 0; i < items; i++) {
            var replaceItemTemplate = '<div class="col-sm-3 col-xs-6">\
            <img src="$url" alt="$name" class="img-thumbnail">\
            <div class="info-wrapper">\
            <div class="text-muted">$name</div>\
            <div class="text-muted top-padding">$description</div>\
            <div class="text-muted">$date</div>\
            </div>\
            </div>';
            
            let resultHTML = replaceItemTemplate
            .replace(/\$name/gi, newData[i].name)
            .replace("$url", newData[i].url)
            .replace("$description", newData[i].description)
            .replace("$date", newData[i].date);
            
            firstBlock.innerHTML += resultHTML;
        }
        document.querySelector('.first-group').classList.remove("hide");
        document.querySelector('.second-group').classList.add("hide");
        document.querySelector('.third-group').classList.add("hide");
    }
    // template strings type
    function tempStrGallery(items) {
        secondBlock.innerHTML = '';
        for (let i = 0; i < items; i++) {
            let secondItemTemplate = `<div class="col-sm-3 col-xs-6">\
            <img src="${newData[i].url}" alt="${newData[i].name}" class="img-thumbnail">\
            <div class="info-wrapper">\
                <div class="text-muted">${newData[i].name}</div>\
                <div class="text-muted top-padding">${newData[i].description}</div>\
                <div class="text-muted">${newData[i].date}</div>\
            </div>\
            </div>`;
            secondBlock.innerHTML += secondItemTemplate;
        }
        document.querySelector('.first-group').classList.add("hide");
        document.querySelector('.second-group').classList.remove("hide");
        document.querySelector('.third-group').classList.add("hide");
    }
    // create items type
    function createGallery(items) {
        console.log(items);
    }

    btn.addEventListener("click", init);

})()