'use strict';

(function(){
    // init variables
    let btn = document.getElementById('add');
    let countEl = document.getElementById('count');
    let imgNum = document.getElementById('imgNum');
    let result = document.getElementById('result');
    let select = document.getElementById('select');
    let count = 0;
    let newData = [];
    let addedImgs = [];
    // refactor data
    data.forEach(item => {
        newData.push({
            url: validateUrl(item.url),
            name: validateName(item.name),
            description: validateDesc(item.description),
            date: validateDate(item.date)
        })
    });
    let allImgNum = newData.length;
    imgNum.innerHTML = allImgNum;
    // check the localStorage
    if (localStorage.getItem('sort')) {
        select.value = localStorage.getItem('sort');
    }
    // validate url
    function validateUrl(url) {
        if (url.includes('http://')) {
            return url;
        } else {
            return `http://${url}`;
        }
    }
    // validate the name
    function validateName(name) {
        if (name.length > 0) {
            return name[0].toUpperCase()+name.slice(1).toLowerCase();
        }
    }
    // validate the description
    function validateDesc(desc) {
        if (desc.length >= 15) {
            return desc.substr(0, 15) + '...'
        } else {
            return desc;
        }
    }
    // validate the date
    function validateDate(date) {
        return moment(date).format('YYYY/M/DD h:mm');
    }
    // add images
    function addImg() {
        // delete item from the newDate array
        let delItem = newData.splice(0, 1);
        // added item from the newData[] to the addedImgs[]
        addedImgs = addedImgs.concat(delItem);
        // sort new array
        sort();
        // rebuild the gallery with the new array
        tempStrGallery(addedImgs);
        // update the counter
        updateCount('plus');
    }
    // remove images
    function removeImg(event) {
        if (event.target.classList.contains('btn-danger')) {
            // get the number of the item to delete
            let removedImgNum = event.target.closest('.col-sm-3').getAttribute('data-item');
            // item to delete
            let delItem = addedImgs.splice(removedImgNum, 1);
            // add deleted item back to initial array
            newData = newData.concat(delItem);
            // rebuild the gallery without deleted item
            tempStrGallery(addedImgs);
            // update the counter
            updateCount('minus');
        }
    }
    // update images count
    function updateCount(type) {
        if (type === 'plus') {
            count += 1;
        } else if (type === 'minus') {
            count -= 1;
        }
        if (count === allImgNum) {
            btn.disabled = true;
        } else {
            btn.disabled = false;
        }
        countEl.innerHTML = count;
    }
    // template strings type
    function tempStrGallery(arr) {
        result.innerHTML = '';
        for (let i = 0; i < arr.length; i++) {
            result.innerHTML += `<div data-item="${i}" class="col-sm-3 col-xs-6">
            <img src="${arr[i].url}" alt="${arr[i].name}" class="img-thumbnail">
            <div class="info-wrapper">
                <div class="text-muted">${arr[i].name}</div>
                <div class="text-muted top-padding">${arr[i].description}</div>
                <div class="text-muted">${arr[i].date}</div>
                <button class="btn btn-danger">Удалить</button>
            </div>
            </div>`;
        }
    }
    // sort
    function sort() {
        if (select.value === 'value1') {
            addedImgs.sort(ascName);
        } else if (select.value === 'value2') {
            addedImgs.sort(descName);
        } else if (select.value === 'value3') {
            addedImgs.sort(ascTime);
        } else if (select.value === 'value4') {
            addedImgs.sort(descTime);
        }
        tempStrGallery(addedImgs);
    }
    function saveSort() {
        localStorage.setItem('sort', this.value);
    }
    // sort by Name A-Z
    function ascName(a, b) {
        if (a.name > b.name) {
            return 1;
        } else if (a.name < b.name) {
            return -1;
        }
    }
    // sort by Name Z-A
    function descName(a, b) {
        if (a.name > b.name) {
            return -1;
        } else if (a.name < b.name) {
            return 1;
        }
    }
    // new items first
    function ascTime(a, b) {
        if (moment(a.date).valueOf() > moment(b.date).valueOf()) {
            return -1;
        } else if (moment(a.date).valueOf() < moment(b.date).valueOf()) {
            return 1;
        }
    }
    // old items first
    function descTime(a, b) {
        if (moment(a.date).valueOf() > moment(b.date).valueOf()) {
            return 1;
        } else if (moment(a.date).valueOf() < moment(b.date).valueOf()) {
            return -1;
        }
    }
    // Events listeners
    btn.addEventListener('click', addImg);
    result.addEventListener('click', removeImg);
    select.addEventListener('change', sort);
    select.addEventListener('change', saveSort);

})();