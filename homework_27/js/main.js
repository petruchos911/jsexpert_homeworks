/* 
*  Схема инициализации приложения
*/
let allBlocks = document.getElementsByTagName('main');
let blockUserInfo = document.querySelector('.block_user-info');
let blockLogin = document.querySelector('.block_login');
let blockGallery = document.querySelector('.block_gallery');
let inputEmail = document.getElementById('inputEmail');
let inputPass = document.getElementById('inputPassword');
let menu = document.querySelector('.js-menu');
let menuGallery = document.querySelector('.nav-link_gallery');
let menuUserInfo = document.querySelector('.nav-link_user');
let menuExit = document.querySelector('.js_exit-btn');
let submitBtn = document.querySelector('.js-submit');
let userEmail = document.getElementById('userEmail');
let userPass = document.getElementById('userPass');
let togglePassBtn = document.getElementById('togglePass');

let validatorModule = new Validator();

let galleryModule = new BaseGallery();
//let galleryModule = new ExtendedGallery();

let loginForm = new LoginForm(validatorModule, galleryModule);
loginForm.initComponent({login: 'admin@email.com', pass: '12345678'});

// Listeners
submitBtn.addEventListener('click', submit);
menuGallery.addEventListener('click', openGallery);
menuUserInfo.addEventListener('click', openUserInfo);
menuExit.addEventListener('click', exit);

function openGallery(e) {
    e.preventDefault;
    loginForm.showGallery();
}
function openUserInfo(e) {
    e.preventDefault;
    loginForm.showUserInfo();
}
function exit(e) {
    e.preventDefault;
    loginForm.exit();
}
function submit(e) {
    e.preventDefault();
    loginForm.validateUserData(inputEmail, inputPass);
}

if (localStorage.page != '') {
    if (localStorage.page === 'gallery') {
        loginForm.showGallery();
    } else if (localStorage.page === 'user') {
        loginForm.showUserInfo();
    }
}