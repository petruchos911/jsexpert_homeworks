/* 
*  Схематическое изображение класса Логин формы
*/

let LoginForm = function(validatorModule, galleryModule) {	
	this.validator = validatorModule;
	this.gallery = galleryModule;
}

LoginForm.prototype = {
	initComponent: function(obj) {
		// code
		localStorage.setItem('login', obj.login);
		localStorage.setItem('pass', obj.pass);
	},
	validateUserData: function(inputEmail, inputPass) {
		if (this.validator.isValid(inputEmail, inputPass) === true) {
			this.showGallery();
		}
	},
	showGallery: function() {
		this.gallery.init();
		Array.from(allBlocks).forEach(function(el) {
			el.classList.add('hide');
		});
		blockGallery.classList.remove('hide');
		menu.classList.remove('d-none');
	},
	showUserInfo: function() {
		Array.from(allBlocks).forEach(function(el) {
			el.classList.add('hide');
		});
		blockUserInfo.classList.remove('hide');
		menuGallery.classList.remove('text-primary');
		menuUserInfo.classList.add('text-primary');
		menu.classList.remove('d-none');
		function togglePass() {
			if (userPass.type === 'password') {
				userPass.type = 'text';
				this.innerText = 'Hide';
			} else {
				userPass.type = 'password';
				this.innerText = 'Show';
			}
		}
		userEmail.value = localStorage.login;
		userPass.value = localStorage.pass;
		togglePassBtn.addEventListener('click', togglePass);
		localStorage.page = 'user';
	},
	exit: function() {
		Array.from(allBlocks).forEach(function(el) {
			el.classList.add('hide');
		});
		blockLogin.classList.remove('hide');
		menu.classList.add('d-none');
		localStorage.page = '';
	}
}
