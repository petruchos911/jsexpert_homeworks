let Validator = function() {
    
}

Validator.prototype = {
    isValid: function(inputEmail, inputPass) {
        function validateEmail(email) {
            let pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
            return pattern.test(email);
        }
        function showHideError(text, action = false) {
            let alert = document.querySelector('.alert');
            if (action === true) {
                alert.classList.add('hide');
            } else {
                alert.innerHTML = text;
                alert.classList.remove('hide');
            }
        }
        function checkCredentials(userEmail, userPass) {
            if (userEmail === localStorage.login && userPass === localStorage.pass) {
                return true;
            } else return false;
        }
        if (inputEmail.value && inputPass.value !== '') {
            if (validateEmail(inputEmail.value) === true) {
                if (inputPass.value.length >= 8) {
                    if (checkCredentials(inputEmail.value, inputPass.value) === true) {
                        console.log('true');
                        showHideError('', true);
                        return true;
                    } else {
                        showHideError('Wrong login or password');
                    }
                } else {
                    showHideError('Password is too short');
                }
            } else {
                showHideError('Email is not valid');
            }
        } else {
            showHideError('fill all fields');
        }
    }
}