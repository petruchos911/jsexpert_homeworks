var btn = document.getElementById("play");
var player1 = document.getElementById("player1");
var player2 = document.getElementById("player2");
var result = document.getElementById("result");

function getPlayerResult() {
    return Math.floor((Math.random() * 3) + 1);
}
function getNameById(num) {
    let name = '';
    if (num === 1) {
        name = 'камень';
    } else if (num === 2) {
        name = 'ножницы';
    } else {
        name = 'бумага';
    }
    return name;
}
function determineWinner(num1, num2) {
    if (num1 === num2 - 1 || num1 === num2 + 2) {
        return 1;
    } else if (num2 === num1 - 1 || num2 === num1 + 2) {
        return 2;
    } else {
        return 0;
    }
}
function printResult(number) {
    let player = '';
    message = '';
    if (number === 0) {
        message = 'ничья';
    } else {
        if (number === 1) {
            player = 'первый';
        } else if (number === 2) {
            player = 'второй';
        }
        message = 'выиграл ' + player + ' игрок';
    }
    result.innerHTML = message;
}

function runGame() {
    let winnner;
    let value1 = getPlayerResult();
    let value2 = getPlayerResult();

    player1.innerHTML = getNameById(value1);
    player2.innerHTML = getNameById(value2);

    winner = determineWinner(value1, value2);

    printResult(winner);
}

btn.addEventListener("click", runGame);