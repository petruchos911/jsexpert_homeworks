'use strict';

function start() {
    const login = new Component();
    login.setLogAndPass('admin@e.com', '123');
    let submitBtn = document.querySelector('.js-submit');

    submitBtn.addEventListener('click', submit);

    function submit(e) {
        e.preventDefault();
        login.init();
    }
}

document.addEventListener('DOMContentLoaded', start);
