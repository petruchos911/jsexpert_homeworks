'use strict';

function Component() {
    // init variables
    let inputEmail = document.getElementById('inputEmail');
    let userEmail = document.getElementById('userEmail');
    let inputPass = document.getElementById('inputPassword');
    let userPass = document.getElementById('userPass');
    let alert = document.querySelector('.alert-danger');
    let formSignIn = document.querySelector('.form-signin');
    let formUserInfo = document.querySelector('.form-userinfo');
    let togglePassBtn = document.getElementById('togglePass');
    let backBtn = document.getElementById('back');
    // private, validate the email
    function validateEmail(email) {
        let pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
        return pattern.test(email);
    }
    // private, check the credentials
    function checkCredentials(userEmail, userPass) {
        if (userEmail === localStorage.login && userPass === localStorage.pass) {
            return true;
        } else return false;
    }
    // private, show/hide password
    function togglePass() {
        if (userPass.type === 'password') {
            userPass.type = 'text';
            this.innerText = 'Hide';
        } else {
            userPass.type = 'password';
            this.innerText = 'Show';
        }
    }
    // private, got back
    function goBack() {
        formSignIn.classList.remove('hide');
        formUserInfo.classList.add('hide');
    }
    this.setLogAndPass = function(login, pass) {
        localStorage.setItem('login', login);
        localStorage.setItem('pass', pass);
    }
    this.init = function() {
        if (validateEmail(inputEmail.value) && inputPass.value !== '') {
            if (checkCredentials(inputEmail.value, inputPass.value) === true) {
                formSignIn.classList.add('hide');
                formUserInfo.classList.remove('hide');
                userEmail.value = localStorage.login;
                userPass.value = localStorage.pass;
            }
        } else {
            alert.classList.remove('hide');
        }

        togglePassBtn.addEventListener('click', togglePass);
        backBtn.addEventListener('click', goBack);
    }
}
