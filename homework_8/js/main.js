var total = 0;

for (var i = 0; i < 15; i++) {
	if (i == 8 || i == 13) {
		continue;
	} else {
		var first = Math.floor((Math.random() * 6) + 1);
		var second = Math.floor((Math.random() * 6) + 1);

		var str = 'Первая кость:' + first + ' Вторая кость:' + second + '; ';

		if (first == second) {
			str += 'Выпал дубль. Число '+ first + ' ';
		}
		if (first < 3 && second > 4) {
			var diff = second - first;
			str += 'Большой разброс между костями. Разница составляет {'+ diff + '}';
		}
		str += '<br>'

		total += first + second;

		document.getElementById("result").innerHTML += str;
	}

}

total > 100 ? document.getElementById("result").innerHTML += 'Победа, вы набрали очков ' + total :  document.getElementById("result").innerHTML += 'Вы проиграли, у вас очков ' + total;
