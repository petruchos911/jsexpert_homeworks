'use strict';

function start() {
    let submitBtn = document.querySelector('.js-submit');
    
    component.set('admin@email.com', '1234');

    submitBtn.addEventListener('click', submit);

    function submit(e) {
        e.preventDefault();
        component.init();
    }
}

document.addEventListener('DOMContentLoaded', start);
