'use strict';

var btn = document.getElementById("play");

data.splice(5, 1);

function transform() {
    var newArr = [];
    data.forEach(item => {
        newArr.push({
            url: item.url,
            name: item.name,
            params: {
                status: item.params.status,
                progress: item.params.progress
            },
            description : item.description,
            date : item.date
        })
    });

    var updNewArr = newArr.map(function(item) {
        return {
            url: `http://${item.url}`,
            name: item.name.charAt(0).toUpperCase()+item.name.slice(1).toLowerCase(),
            description: item.description.substr(0, 15) + '...',
            date: moment(item.date).format('YYYY/M/DD h:mm'),
            params: `${item.params.status}=>${item.params.progress}`,
            isVisible: item.params.status
        }
    });

    var filteredArr = updNewArr.filter(item => item.isVisible === true);

    printResult(filteredArr);

    function printResult(arr) {
        console.log(arr);
    }
}

btn.addEventListener("click", transform);