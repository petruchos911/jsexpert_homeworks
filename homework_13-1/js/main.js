(function() {
    var total = 0,
        first,
        second,
        str = '';

    for (let i = 0; i < 15; i++) {
        if (i == 8 || i == 13) {
            continue;
        } else {
            first = getRndNumber();
            second = getRndNumber();

            setResult('Первая кость:' + first + ' Вторая кость:' + second + ';<br>');

            isNumbersEqual(first, second);

            isBigDifference(first, second);

            getTotal(first, second);
        }
    }

    printResult();

    function getRndNumber() {
        return Math.floor((Math.random() * 6) + 1);
    }
    function setResult(string) {
        return str += string;
    }
    function isNumbersEqual(num1, num2) {
        if (num1 == num2) {
            setResult('Выпал дубль. Число '+ num1 + ' <br>');
        }
    }
    function isBigDifference(num1, num2) {
        if ((num1 < 3 && num2 > 4) || (num2 < 3 && num1 > 4)) {
            setResult('Большой разброс между костями. Разница составляет {'+ Math.abs(num1 - num2) + '}' + '<br>');
        }
    }
    function getTotal(num1, num2) {
        total += num1 + num2;
    }
    function printResult() {
        setResult((total > 100 ? 'Победа, вы набрали очков ' : 'Вы проиграли, у вас очков ') + total);
        document.getElementById("result").innerHTML = str;
    }
}());